# Name  : run a playbook
# TAG   : ansible running

```
ansible-playbook -i <INVENTORY> <PLAYBOOK>  -k -K
ansible-playbook -i inventories/production/hosts -l server1,server5 playbook.yml -k -K
ansible-playbook -i inventories/preprod/hosts -l web playbooks/playbook.yml -k -K
ansible-playbook -i inventories/preprod/hosts -l db,server1 steve.yml -k -K
```

