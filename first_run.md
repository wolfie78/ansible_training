# Name: ansible first_run
# TAG: ansible first run

soooo we are going to quickly create a quick playbook and run it, this will install software and
start and enable it.
```
---
- name: simple | simple yml file
  hosts: server1,server2
  remote_user: USERID
  gather_facts: false
  become: true
  
  tasks:
    - name: install httpd
      dnf:
        name: httpd
        state: present
      
    - name: start and enable httpd
      systemd:
        name: httpd
        state: started
        enabled: true
```
now to run this is:
`$ ansible-playbook -i "server," simple.yml -k -K`

# Questions
1. Can you see the inventory?
2. Can you see the role?
3. Where is the playbook?
4. Can you see a handler?
5. Can you see the remote user?
