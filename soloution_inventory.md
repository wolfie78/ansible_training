# Name:  Soloution Inventory
# TAG: soloution inventory

# before: `simple.yml`
```
- name: simple | simple yml file
  hosts: all
  rmote_user: USERID
  gather_facts: false
  
  tasks:
    - name: install httpd
      dnf:
        name: httpd
        state: present
      
    - name: start and enable httpd
      systemd:
        name: httpd
        state: started
        enabled: true
```
# after: 
## file: `simple.yml`
```
- name: simple | simple yml file
  hosts: all
  ansible_user: USERID
  gather_facts: false
  
  tasks:
    - name: install software
      dnf:
        name: "{{ software_install }}"
        state: present
      
    - name: remove software
      dnf:
        name: "{{ software_remove }}"
        state: absent
      when: software_remove is defined
      
    - name: start and enable httpd
      systemd:
        name: httpd
        state: started
        enabled: true
```

## file: `inventory/hosts`
```
[serverlist]
server1
server2
```

## file: `inventory/group_vars/all/all.yml`
```
software_install:
  - httpd
  - htop

software_remove: []
```

## running
`$ ansible-playbook -i inventory/hosts simple.yml -k -K`
