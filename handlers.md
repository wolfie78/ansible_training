# Name: handlers
# TAG: ansible handlers

in your task add a notify:
```
- name: simple ansible
  yum:
    name: httpd
    state: latest
  notify:
    - start enable httpd
```
in `role/simple/handlers/main.yml` you will then have:
```
---
- name: start enable httpd
  systemd:
    name: httpd
    state: started
    enabled: true
```


