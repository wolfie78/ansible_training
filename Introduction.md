# Name  : Introduction
# TAG   : ansible introduction

# Links:
| Note                              | link                                                                                 |
| Main ansible site                 | `https://www.ansible.com/`                                                           |
| Main doc site                     | `https://docs.ansible.com/`                                                          |
| List of ansible collections       | `https://docs.ansible.com/ansible/latest/collections/index.html#list-of-collections` |

# What is ansible
Ansible is an open source automation tool to configure servers and software, although you can also deploy whole
servers, physical or virtual.

