# Name: soloution roles
# TAG: 


## file: `playbooks/simple.yml`
```
- name: simple | simple yml file
  hosts: all
  ansible_user: USERID
  gather_facts: false
  
  roles:
    - simple
```

## file: `inventory/hosts`
```
[serverlist]
server1
server2
```

## file: `inventory/group_vars/all/all.yml`
```
software_install:
  - httpd

software_remove: []
```

## file: `roles/simple/tasks.yml`
```
---
- name: install software
  dnf:
    name: "{{ software_install }}"
    state: present
  notify:
    - start enable httpd
      
- name: remove software
  dnf:
    name: "{{ software_remove }}"
    state: absent
  when: software_remove is defined
```

## file: `role/simple/handlers/main.yml` 
```
---
- name: start enable httpd
  systemd:
    name: httpd
    state: started
    enabled: true
```

## running
`$ ansible-playbook -i inventory/hosts playbooks/simple.yml -k -K`

