# Name  : Inentories
# TAG   : ansible inventories

if we take this setup:
```
inventories/                    # list of inventories
└── production/                 # production inventory folder
    ├── group_vars/             # Production group_vars folder
    │   │── all/                # All folder, everying in there is 
    │   │   └─── test1.yml      # file for all varibles.
    │   │── web.yml             # web group of varibles
    │   └── front.yml           # front group of varybles
    ├── host_vars/              # Production host_vars folder
    │   └── server1.yml         # server1 varible file.
    └── hosts                   # Inventory file with a list of servers in certain groups.
```
a sample `hosts` file could be like this:
```
[prod]
server1
server2
server3

[web]
server2

[front]
server1
server2
```

and these are the varible files which will be applied:
| server  | group_vars/all/test.1.yml  | group_vars/web.yml    | group_vars/front.yml   |
| ------- | -------------------------- | --------------------- | ---------------------- |
| server1 | yes                        | no                    | yes                    |
| server2 | yes                        | yes                   | yes                    |
| server3 | yes                        | no                    | no                     |

## Things to do:
Now we know a bit about about our simple playbook we can do a couple of things:
1. Create an inventory file with our host listed in it.
2. Create a group vars with a list of software you would like installed

## My soloution
[soloution](soloution_inventory.md)
