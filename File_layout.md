# Name  : File layout
# TAG   : ansible layout.
# Notes :

For more information you can have a look at:
`https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#directory-layout`

but here is a summary:
```
ansible_project/                    # Project name
├── inventories/                    # list of inventories
│   ├── production/                 # production inventory folder
│   │   ├── group_vars/             # Production group_vars folder
│   │   │   └── group1.yml          # Group var file with varibles.
│   │   ├── host_vars/             # Production host_vars folder
│   │   │   └── server1.yml         # server1 varible file.
│   │   └── hosts                   # Inventory file with a list of servers in certain groups.
│   └── staging/
│       └── hosts
├── playbook.yml                    # Playbook that contains a list of roles to run, generally I
│                                   # like them in a playbook folder, but that is just my preference.
└── roles/                          # roles folder
    └── example/                    # a role ccalled example
        ├── defaults/               # defaults for the role
        ├── files/                  # A list of files that you will copy to the designated server.
        ├── handlers/               # handlers folder, this will be a list of services etc. that you
        │                           # want to use, the default file is called main.yml
        ├── meta/                   # I have never used, but you have a main.yml and in there you
        │                           # can list dependencies
        ├── tasks/                  # REQUIRED : tasks folder, this is a must for a role
        │   └── main.yml            # REQUIRED : main.yml is a list of tasks
        ├── templates/              # templates folder which has a list of templates in there
        └── vars/                   # A list of other vars for the role
```

According to Ansible’s definition, the difference between defaults and vars is:

1. defaults mean “default variables for the roles” and vars mean “other variables for the role”.
2. The priority of the vars is higher than that of defaults.
      For example, consider a variable named ‘version’ defined in the defaults have value ‘5.0.1’ 
      and the same variable defined in vars have value ‘7.1.3’, so the final value that will be 
      taken into account is the value defined in vars i.e., ‘7.1.3’.
