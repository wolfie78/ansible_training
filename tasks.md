# Name: ansible tasks
# TAG: tasks ansible
# Notes:

# Todo
1. Create an ssh-key and using the `authorized key` ansible module put it on the server for the
   ansible user
2. use `lineinfile` module to set port to `8080` in `/etc/httpd/conf/httpd.conf` 
3. open up port 8080 using the `firewalld`
4. Using the `user` module create a user called `harrold` and give access to the server
5. varible'ize it and set defaults.
6. have a look at `when` command and `ansible -m setup localhost` you can do a bit of greping
7. use `copy` module to copy a file to the server
8. use the `template` module for the `httpd.conf` file and varibalize the port number
9. also update the `firewalld` to add a port
10. Seperate the firewall for the software in the roles section
