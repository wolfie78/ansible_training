#!/bin/bash

# Define the project directory and role name
project_dir="ansible_project"
role_name="example"

# Create the project directory
mkdir -p "$project_dir"

# Create the required subdirectories and files
cd "$project_dir" || exit

# Directory structure
mkdir -p roles/"$role_name"/{defaults,files,handlers,meta,tasks,templates,vars}
mkdir -p inventories/{production,staging}
touch inventories/{production,staging}/hosts

# Create a sample playbook
cat <<EOL > playbook.yml
---
- name: Example Playbook
  hosts: all
  become: yes
  tasks:
    - name: Ensure the example role is applied
      include_role:
        name: $role_name
EOL

# Create a sample role files
cat <<EOL > roles/$role_name/tasks/main.yml
---
- name: Example Task
  debug:
    msg: "Hello from the $role_name role!"
EOL

# Provide some instructions
echo "Ansible project layout with the '$role_name' role has been created in '$project_dir'."
echo "You can now start adding your inventory, variables, and additional roles as needed."
