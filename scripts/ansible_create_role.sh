#!/bin/bash

# Check if the user provided a role name as an argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <role_name>"
    exit 1
fi

# Define the role name provided as an argument
role_name="$1"

# Check if the role already exists
if [ -d "roles/$role_name" ]; then
    echo "Role '$role_name' already exists in the project directory."
    exit 1
fi

# Create the role directory structure
mkdir -p "roles/$role_name"/{defaults,files,handlers,meta,tasks,templates,vars}

# Create a sample main task file
cat <<EOL > "roles/$role_name/tasks/main.yml"
---
- name: Example Task for $role_name
  debug:
    msg: "Hello from the $role_name role!"
EOL

echo "Role '$role_name' has been created in the 'roles' directory."

