# Name  : playbooks
# TAG   : ansible playbooks

A playbook is what we run to kick everything off, it is where we can say what roles run, what
servers we run against, any particular varibles we could add etc... etc...

Here is an example:
```
---
- name: example playbook
  vars:
    ansible_python_interpreter: "/usr/bin/env python3"
  hosts: all
  become: yes
  
  roles:
    - example
```



