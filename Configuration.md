# Name  : Configuration
# TAG   : ansible configuration

# Notes
Generally ansible uses a file called `ansible.cfg` this is usually in the ansible projects root
directory and there you can set multiple options.

To see a full option you can do:
```
$ ansible-config init --disabled > ansible.cfg
```

This will give you a full list of options you can change, the only cahnge I would make straight away
is under `[defaults]` is fine the roles_path and change it to just `roles` i.e.:
```
# additional paths to search for roles in, colon separated
roles_path = roles
```
