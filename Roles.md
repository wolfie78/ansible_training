# Name  : Roles
# TAG   : ansible roles

A role is a series of tasks that are organised into a collection, we call it a role, here is an example of a role called `example`:

```
roles/                          # roles folder
└── example/                    # a role ccalled example
    ├── defaults/               # defaults for the role
    │   └── main.yml            # 
    ├── files/                  # A list of files that you will copy to the designated server.
    │   └── server.conf         # 
    ├── handlers/               # handlers folder, this will be a list of services etc. that you
    │   └── main.yml            # 
    ├── meta/                   # I have never used, but you have a main.yml and in there you
    │   └── main.yml            # can list dependencies
    ├── tasks/                  # REQUIRED : tasks folder, this is a must for a role
    │   └── main.yml            # REQUIRED : main.yml is a list of tasks
    ├── templates/              # templates folder which has a list of templates in there
    │   └── httpd.conf.j2       # 
    └── vars/                   # A list of other vars for the role
        └── main.yml            # 
```

## defaults
You are required to have a `main.yml` file in this folder, in here you can add default variblesc,
i.e. if you might have 2 sites with different settings for say ntp, you could pick defaults.

## files
Any files that you want to copy to the server.

## [handlers](handlers.md)
We call handlers from a task.
i.e. we have done this, but only if we have done that, we need to do this as well.

## meta
metadata for the role, including role dependencies and optional Galaxy metadata such as platforms
supported.

## tasks
you need to have a `main.yml` in here and this is the main place that gets executed when you run it.

## templates
Here you keep templates that use jinja2 templating module to put onto the server

## vars
According to Ansible’s definition, the difference between defaults and vars is:
  1. defaults mean “default variables for the roles” and vars mean “other variables for the role”.
  2. The priority of the vars is higher than that of defaults.
      For example, consider a variable named ‘version’ defined in the defaults have value ‘5.0.1’ 
      and the same variable defined in vars have value ‘7.1.3’, so the final value that will be 
      taken into account is the value defined in vars i.e., ‘7.1.3’.

## THINGS TO DO
1. now turn your `simple.yml` into a role and playbook
2. Use a handler to restart the httpd if it gets updated.
[soloution_roles](soloution_roles.md)

