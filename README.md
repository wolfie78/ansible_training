# Ansible 101

This is for ansible learning from scratch, general knowledge about how to install / configure and
use.

1. [Introduction](Introduction.md)   - What is ansible
2. [Installation](Installation.md)   - How to install (Red Hat based distros)
3. [Configuration](Configuration.md)  - How to generally configure Ansible
4. [first_run](first_run.md)      - How to run ansible 
5. [Inventories](Inventories.md)    - How they fit there
6. [File_layout](File_layout.md)    - Default filesystem layout
7. [Playbooks](Playbooks.md)      - How to configure a playbook
8. [Roles](Roles.md)          - Role layouts and rules
9. [Running](Running.md)        - Running ansible
10. [tasks](tasks.md)         - some simple tasks
