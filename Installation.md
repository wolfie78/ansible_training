# Name  : Instalation
# TAG   : ansible install

# Introduction
Going to make this short and sweet, I am only really concenrned with redhat based distros, so it is
either:
```
$ dnf install ansible
```
or
```
$ dnf install ansible-core
```

## ansible
This is a community edition which uses a slightly different versioning and will contain certain
ansible collections.

## ansible-core
This uses ansible classic versioning and only ansible.builtin collection.
